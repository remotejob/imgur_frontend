import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import configureStore from './store/configureStore';

const store = configureStore();
const mount = document.getElementById('mount')

 const render = () =>  ReactDOM.render(
   
    <App store={store} />,mount
  )

render()
store.subscribe(render)
