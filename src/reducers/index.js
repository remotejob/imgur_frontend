import { combineReducers } from 'redux';
import * as types from '../actions/types';

const dbdata = (state = {}, action) => {
	switch (action.type) {
		case types.LOAD_DATA_SUCCESS:
			// console.log("LOAD_DATA_SUCCESS",action)
			state = action.data
			return state
		case types.LOAD_ONE_OBJ_SUCCESS:
			//		console.log("LOAD_ONE_OBJ_SUCCESS")
			state = action.data
			return state
		default:
			return state
	}
};


const initialState = {
	visible: false,
	selectedImage: {}
}

const modalstatus = (state , action) => {

	if (typeof state === 'undefined') {
		return initialState
	}

	switch (action.type) {
		case types.SHOWMODAL:
			
			state = action
			return state

		case types.CLOSEMODAL:
			
			state = action
			return state

		default:
			return state
	}

};

const rootReducer = combineReducers({
	dbdata,
	modalstatus

});

export default rootReducer;

