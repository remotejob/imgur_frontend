import * as types from './types';


export function loadalldata(imgtype) {

    var myHeaders = new Headers();
    myHeaders.append("X-Thumbnails-Header", imgtype);

    var myInit = {
        method: 'GET',
        headers: myHeaders,
        mode: 'cors',
        cache: 'default'
    };

    return dispatch => fetch(`http://159.203.134.218:31323`, myInit) // Redux Thunk handles these
        .then(res => res.json())
        .then(
        data => dispatch({ type: types.LOAD_DATA_SUCCESS, data })
        //      err => dispatch({ type: 'LOAD_DATA_FAILURE', err })
        );

}


export function showmodal(visible,img) {
    console.log(visible)
    return {
        type: types.SHOWMODAL,
        visible : visible,
        selectedImage: img
    };
}

export function closemodal(visible,img) {

        return {
        type: types.CLOSEMODAL,
        visible : visible,
        selectedImage: img
        
    };


}


// export function selectImg(imageselected) {
//     console.log("selectImg", imageselected)

//     return {
//         type: types.SELECTIMAGE,
//         imageselected
//     };
// }