import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, Image, Modal } from 'react-bootstrap';
import { loadalldata } from './actions';
import { showmodal } from './actions';
import { closemodal } from './actions';

/**
 * 
 */

const typeOfImage = "b"
const wellStyles = { maxWidth: '60%', margin: '0 auto 10px' };

const App = ({loadDataClick, openModal, dbdata, modalStatus, closeModal}) => {

    var real_obj_html = []

    if (Object.keys(dbdata).length === 0) {

        loadDataClick()
        return null
    } else {

        dbdata.Images.map(image => {
            var selImage = Object.create(image);
            selImage.link = selImage.link.replace(typeOfImage + ".", ".")
            real_obj_html.push(<Image key={image.id} onClick={() => { openModal(selImage) } } src={image.link} thumbnail></Image>)

        })

    }

    return (

        <div>
            <div className="well" style={wellStyles}>

                {real_obj_html}

            </div>
            <Modal show={modalStatus.visible} >
                <Modal.Header>
                    <Modal.Title>{modalStatus.selectedImage.title}</Modal.Title>

                </Modal.Header>
                <Modal.Body>

                    <p>Score:{modalStatus.selectedImage.score}Ups:{modalStatus.selectedImage.ups}Downs:{modalStatus.selectedImage.downs}</p>
                    <Image src={modalStatus.selectedImage.link} thumbnail></Image>

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => { closeModal(modalStatus.selectedImage) } }>Close</Button>
                </Modal.Footer>
            </Modal>

        </div>

    );
};


const mapStateToProps = (state) => {

    return {

        dbdata: state.dbdata,
        modalStatus: state.modalstatus

    };
};

const mapDispatchToProps = (dispatch) => {

    return {
        loadDataClick: () => dispatch(loadalldata(typeOfImage)),
        openModal: (par) => dispatch(showmodal(true, par)),
        closeModal: (par) => dispatch(closemodal(false, par))

    };
};

export default connect(

    mapStateToProps,
    mapDispatchToProps

)(App);
